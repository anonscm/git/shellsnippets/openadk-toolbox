#ifndef LIBOADK_STDIO_H
#define LIBOADK_STDIO_H

#include_next <stdio.h>

char *fgetln(FILE *, size_t *);

#endif
