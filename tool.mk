SRCS?=		${PROG:=.c}

include ./common.mk

CPPFLAGS+=	-I../src
LDFLAGS+=	-L../lib
LIBS+=		-loadk_toolbox
VPATH=		../src

all: ${PROG}

${PROG}: ${OBJS}
	${CC} ${CFLAGS} ${LDFLAGS} -o $@ \
	    -Wl,--start-group ${OBJS} ${LIBS} -Wl,--end-group

DESTDIR?=
BINDIR?=/bin
INSTALL?=/usr/bin/install
INSTALL_STRIP?=-s

install:
	${INSTALL} -c ${INSTALL_STRIP} -m 755 ${PROG} ${DESTDIR}${BINDIR}/
